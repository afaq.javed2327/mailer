import Head from "next/head";
import Image from "next/image";
import { useEffect, useState } from "react";

export default function Home() {
  const [jobStatus, setJobStatus] = useState({
    job: "",
    mailSent: 0,
  });
  const [mailCounter, setMailCounter] = useState(0);
  const [initial, setInitial] = useState(true);

  useEffect(() => {
    const firstInput = document.getElementsByTagName(
      "input"
    )[0] as HTMLInputElement;
    firstInput.focus();
    firstInput.select();
  }, []);

  const getJobStatus = (id: string) => {
    var requestOptions = {
      method: "GET",
      redirect: "follow",
    };

    //@ts-ignore
    fetch(`http://localhost:4000/mail-status/${id}`, requestOptions)
      .then((response) => response.json())
      .then((res) => {
        setJobStatus({
          ...jobStatus,
          job: res?.jobStatus,
          mailSent: res?.mailSent,
        });
      })
      .catch((error) => console.log("error", error));
  };

  useEffect(() => {
    const JobId = localStorage.getItem("jobId");
    if (JobId) {
      if (initial) {
        getJobStatus(JobId);
        setInitial(false);
      } else {
        if (jobStatus.job == "Active") {
          setTimeout(() => {
            getJobStatus(JobId);
          }, 5000);
        }
      }
    }
  }, [jobStatus]);

  const handleSubmit = (e: any) => {
    e.preventDefault();
    var myHeaders = new Headers();
    myHeaders.append("Content-Type", "application/json");

    var raw = JSON.stringify({
      mailCount: mailCounter,
    });

    var requestOptions = {
      method: "POST",
      headers: myHeaders,
      body: raw,
      redirect: "follow",
    };
    //@ts-ignore
    fetch("http://localhost:4000/start-campaign", requestOptions)
      .then((response) => response.json())
      .then((res) => {
        localStorage.setItem("jobId", res?.jobId);
        setJobStatus({
          ...jobStatus,
          job: "Active",
          mailSent: 0,
        });
      })
      .catch((error) => console.log("error", error));
  };

  return (
    <>
      <Head>
        <title>Mailer</title>
      </Head>
      <main className="w-full h-[100vh] flex justify-center items-center">
        <div className="bg-white w-full h-full xl:w-[30%] xl:h-[70vh] border rounded-xl flex flex-col items-center justify-center">
          <div className="w-[70%] h-[80%] flex flex-col items-center justify-around">
            <Image width={200} height={100} src={"/logo.png"} alt="logo" />
            <form
              onSubmit={handleSubmit}
              className="text-black my-6 w-full h-[50%] gap-8 flex flex-col"
            >
              {jobStatus.job != "" && (
                <div className="flex flex-col gap-4">
                  <p>Job Id: {localStorage.getItem("jobId")}</p>
                  <div className="flex justify-between">
                    <p>Job Status: {jobStatus?.job}</p>
                    <p>Mails Sent: {jobStatus?.mailSent}</p>
                  </div>
                </div>
              )}
              <div className="relative">
                <input
                  id="campaign_number"
                  className="theme-input peer"
                  placeholder="Mails Count"
                  onChange={(e) => {
                    setMailCounter(parseInt(e.target.value));
                  }}
                />
                <label
                  htmlFor={"campaign_number"}
                  className="theme-input-label"
                >
                  Mails Count
                </label>
              </div>
              <button
                type="submit"
                className="h-14 bg-[#5e255f] text-white rounded-md transition-all duration-300 disabled:opacity-50"
              >
                Start Campaign
              </button>
            </form>
          </div>
        </div>
      </main>
    </>
  );
}
