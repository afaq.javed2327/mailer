import type { Config } from "tailwindcss";

const config: Config = {
  content: [
    "./src/pages/**/*.{js,ts,jsx,tsx,mdx}",
    "./src/components/**/*.{js,ts,jsx,tsx,mdx}",
    "./src/app/**/*.{js,ts,jsx,tsx,mdx}",
  ],
  theme: {
    extend: {
      fontFamily: {
        theme: ["var(--font-theme)"],
      },
      colors: {
        theme: "#E1A95F",
        "theme-light": "#FBF4EB",
        "theme-red": "#D0312D",
        "theme-green": { 50: "#46D387", 100: "#0E2D1D" },
        "theme-blue": { 50: "#4352DF" },
        "theme-gray": {
          25: "#FAFAFB",
          50: "#F8F8F8",
          75: "#F4F4F4",
          100: "#EDEDED",
          125: "#E8E8E9",
          150: "#DDDDDE",
          175: "#D1D1D2",
          200: "#C6C6C7",
          225: "#BABBBC",
          250: "#AFAFB1",
          275: "#A3A4A6",
          300: "#98999B",
          325: "#8C8D8F",
          350: "#818284",
          375: "#767679",
          400: "#6A6B6E",
          425: "#5F6063",
          450: "#535458",
          475: "#48494C",
          500: "#3C3D41",
          525: "#313236",
          550: "#25272B",
          575: "#1A1B20",
        },
      },
    },
  },
  plugins: [],
};
export default config;
