#!/bin/bash

# Build Docker images
cd mailer-client
docker build -t mailer-client:latest .
cd ../mailer-server
docker build -t mailer-server:latest .
cd ..

# Run Docker Compose
docker-compose up -d

# Wait for services to start (adjust the sleep duration as needed)
sleep 15

# Access services
echo "Frontend running at http://localhost:3000"
echo "Backend running at http://localhost:4000"

# Optionally, open services in default web browser
# Uncomment the lines below if you want to automatically open the services in the default web browser
xdg-open http://localhost:3000
# xdg-open http://localhost:4000
