import { Module } from '@nestjs/common';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { MailerModule } from '@nestjs-modules/mailer';
import { BullModule } from '@nestjs/bull';
import { CampaignerConsumer } from './campaigner.consumer';

@Module({
  imports: [
    MailerModule.forRoot({
      transport: {
        host: 'smtp.gmail.com',
        auth: {
          user: 'afaq@gmail.com',
          pass: '12345678',
        },
      },
    }),

    BullModule.forRoot({
      redis: {
        host: 'localhost',
        port: 6379,
      },
    }),

    BullModule.registerQueue({
      name: 'campaigner',
    }),
  ],
  controllers: [AppController],
  providers: [AppService, CampaignerConsumer],
})
export class AppModule {}
