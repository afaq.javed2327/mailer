import { Process, Processor } from '@nestjs/bull';
import { Job } from 'bull';
import { AppService } from './app.service';

@Processor('campaigner')
export class CampaignerConsumer {
  constructor(private readonly appService: AppService) {}

  @Process()
  async campaign(job: Job<any>) {
    const mailCount = job.data.mailCount.mailCount;
    let progress = 0;
    for (let i = 0; i < mailCount; i++) {
      // Sending Email
      // this.appService.sendMail();
      await new Promise((resolve: any) => setTimeout(resolve, 4000));
      //   console.log(`${i + 1} Mails Sent!`);
      progress += 1;
      await job.progress(progress);
    }

    return {};
  }
}
