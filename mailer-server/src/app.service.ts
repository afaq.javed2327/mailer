import { MailerService } from '@nestjs-modules/mailer';
import { InjectQueue } from '@nestjs/bull';
import { Injectable } from '@nestjs/common';
import { Queue } from 'bull';

@Injectable()
export class AppService {
  constructor(
    private readonly mailerService: MailerService,
    @InjectQueue('campaigner') private readonly campaignerQueue: Queue,
  ) {}
  getHello(): string {
    return 'Hello World!';
  }

  async getMailStatus(id: number): Promise<any> {
    const job = await this.campaignerQueue.getJob(id);

    if (job) {
      return {
        jobStatus: job?.finishedOn ? 'Completed' : 'Active',
        mailSent: job?.progress(),
      };
    } else {
      return {
        jobStatus: 'In Active',
        mailSent: 0,
      };
    }
  }

  sendMail(): object {
    try {
      // this.mailerService.sendMail({
      //   to: 'ragzon@gmail.com',
      //   from: 'afaq@gmail.com',
      //   subject: 'Tech Lead Task',
      //   html: '<h1>Congrats Task Completed!<h1>',
      // });
      return {
        status: 200,
        msg: 'Mail Sent!',
      };
    } catch (error) {
      return {
        status: 400,
        msg: 'Mail Sent Failed!',
      };
    }
  }

  async startCampaignServer(mailCount: number): Promise<any> {
    const job = await this.campaignerQueue.add({
      mailCount: mailCount,
    });

    return {
      jobId: job.id,
    };
  }
}
