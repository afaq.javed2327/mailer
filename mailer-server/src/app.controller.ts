import { Body, Controller, Get, Param, Post } from '@nestjs/common';
import { AppService } from './app.service';

@Controller()
export class AppController {
  constructor(private readonly appService: AppService) {}

  @Get()
  getHello(): string {
    return this.appService.getHello();
  }

  @Get('mail-status/:id')
  getMailStatus(@Param() job: any): object {
    return this.appService.getMailStatus(job.id);
  }

  @Post('start-campaign')
  startCampaign(@Body() mailCount: number): object {
    return this.appService.startCampaignServer(mailCount);
  }
}
