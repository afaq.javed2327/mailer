# Mailer

Mailer is a project that includes a Next.js frontend, a Nest.js backend, and uses Redis for certain functionalities.

## Prerequisites

Before running the project, make sure you have the following installed:

- [Node.js](https://nodejs.org/) (version 14 or later)
- [Docker](https://docs.docker.com/get-docker/)
- [Docker Compose](https://docs.docker.com/compose/install/)
- [Nest CLI](https://docs.nestjs.com/cli)

## Getting Started

To run this project:

1. **Run Deploy Script:**
   ```bash
   chmod +x deploy.sh
   ./deploy.sh
   ```
